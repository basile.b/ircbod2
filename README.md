# ircbod2 [![Build Status](https://travis-ci.org/Basile-z/ircbod2.svg?branch=master)](https://travis-ci.org/Basile-z/ircbod2)

## Information

This library is mostly a refreshed version of [this project](https://github.com/bakkdoor/ircbod).
It allows to easily write IRC bots in D and without any dependency.

`ircbod2` is licensed under BSD-3 clauses license (see LICENSE file).

The `IRCSocket` class was ported from: [the Ruby IRCSocket library](https://github.com/injekt/irc-socket).

## Build example

```sh
cd examples
rdmd -I../source/ hello_bot.d
```

Then go to `#ircbod` on freenode to speak to your bot.

The project can be build as a static library but as shown by the example it's not necessary.