/**
 * This IRC bot simply copies the activity of "#d.feed" and republishes it
 * to the main "#d" room, which is usually only done for new announces,
 * new topics and replies when authored by the D designer or its architect.
 */
module public_dfeeds;

import std.stdio;
import ircbod.client, ircbod.message;

void main()
{
    string lastPublic;

    IRCClient bot = new IRCClient("irc.freenode.net", 6667, "PublicDFeed",
        null, ["#d", "#d.feed"]);

    bot.on(IRCMessage.Type.MESSAGE, (msg, args)
    {
        if (msg.nickname == "DFeed")
        {
            if (msg.channel == "#d.feed" && msg.text != lastPublic)
            {
                bot.sendMessageToChannel(msg.text, "#d");
            }
            else if (msg.channel == "#d")
            {
                lastPublic = msg.text;
            }
        }
    });

    bot.on(IRCMessage.Type.PRIV_MESSAGE, r"^!quit$", (msg)
    {
        msg.reply("Yes, master.");
        bot.broadcast("My master told me to quit. Bye!");
        bot.quit();
    });

    bot.run();
}

